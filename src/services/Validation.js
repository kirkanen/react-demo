export default class Validation {
  validate = (state, target) => {
    let value = target.value, errorMsg = state.errorMsg, showError=true, valid = true, phoneRegex = /^\d+$/,
      emailRegex = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    if (state.validators.includes('required') && value.trim() === '') {
      valid = false;
      showError = state.noRequiredMsg ? false : true;
      errorMsg = state.noRequiredMsg ? state.errorMsg : state.placeholder + ' is required';
    } else if (state.validators.includes('email') && !emailRegex.test(value)) {
      valid = false;
      errorMsg = 'Invalid e-mail address';
    } else if (state.validators.includes('phone') && !phoneRegex.test(value)) {
      valid = false;
      errorMsg = 'Only numbers allowed';
    }
    return {valid: valid, errorMsg: errorMsg, showError: showError};
  }
}
