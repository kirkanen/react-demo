import React from 'react';
import InputComponent from './InputComponent';

export class AddComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: {
        name: 'name',
        placeholder: 'Full name',
        value: '',
        valid: false,
        touched: false,
        validators: ['required'],
        noRequiredMsg: true
      },
      email: {
        name: 'email',
        placeholder: 'E-mail address',
        value: '',
        valid: false,
        touched: false,
        validators: ['required', 'email'],
        noRequiredMsg: true
      },
      phone: {
        name: 'phone',
        placeholder: 'Phone number',
        value: '',
        valid: false,
        touched: false,
        validators: ['required', 'phone'],
        noRequiredMsg: true
      }
    };
  }

  onUpdate = (element) => {
    let newState = {...this.state[element.name]};
    newState.value = element.value;
    newState.valid = element.valid;
    this.setState({
      [element.name]: newState
    });
  }

  update = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onAdd = () => {
    let newContact = {
      name: this.state.name.value,
      email: this.state.email.value,
      phone: this.state.phone.value,
    };
    this.props.onAdd(newContact);
    this.setState({
      name: this.resetInput('name'),
      email: this.resetInput('email'),
      phone: this.resetInput('phone')
    })
  };

  resetInput(name) {
    let input = {...this.state[name]};
    input.value = '';
    input.valid = false;
    input.touched = false;
    return input;
  }

  formValid() {
    return this.state.name.valid && this.state.email.valid && this.state.phone.valid;
  }

  render() {
    return (
      <div className = "table add-table">
        <div className="tr edit">
          <div className = "td col1" >
            <InputComponent model={this.state.name} onUpdate={this.onUpdate}/>
          </div>
          <div className = "td col2" >
            <InputComponent model={this.state.email} onUpdate={this.onUpdate}/>
          </div>
          <div className = "td col3" >
            <InputComponent model={this.state.phone} onUpdate={this.onUpdate}/>
          </div>
          <div className = "td col4" >
            <button disabled={!this.formValid()} onClick={this.onAdd}>Add new</button>
          </div>
        </div>
      </div>
    );
  }
}

export default AddComponent;
