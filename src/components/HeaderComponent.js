import React from 'react';
import LogoComponent from './LogoComponent';

export class HeaderComponent extends React.Component {
  render() {
    return (
      <header className = "header" ><LogoComponent /><div className="title">Nord Software</div></header>
    );
  }
};
export default HeaderComponent;
