import React, { Component } from 'react';
import HeaderComponent from './components/HeaderComponent';
import ContentComponent from './components/ContentComponent';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <HeaderComponent />
        <ContentComponent />
      </div>
    );
  }
}

export default App;
