import React from 'react';
import Validation from '../services/Validation';

export class InputComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: this.props.model.name,
      value: this.props.model.value,
      placeholder: this.props.model.placeholder,
      validators: this.props.model.validators,
      valid: false,
      touched: false,
      errorMsg: null,
      noRequiredMsg: this.props.model.noRequiredMsg || false,
      showError: true
    };
    this.validation = new Validation();
  }

  onUpdate = (e) => {
    let validation = this.validation.validate(this.state, e.target);
    this.setState({
      value: e.target.value,
      valid: validation.valid,
      touched: true,
      errorMsg: validation.errorMsg,
      showError: validation.showError
    }, () => this.props.onUpdate(this.state));
  };

  onBlur = (e) => {
    let validation = this.validation.validate(this.state, e.target);
    this.setState({
      touched: true,
      valid: validation.valid,
      errorMsg : validation.errorMsg,
      showError: validation.showError
    });
  }

  showError() {
    return this.state.touched && !this.state.valid && this.state.showError;
  }

  render() {
    return (
      <div>
        <input
          type="text"
          className={this.showError() ? 'error' : ''}
          name={this.props.model.name}
          placeholder={this.state.placeholder}
          value={this.props.model.value}
          onChange={this.onUpdate}
          onBlur={this.onBlur} />
          <div className={'errorMsg' + (this.showError() ? ' show' : '')}>{this.state.errorMsg}</div>
      </div>
    );
  }
};
export default InputComponent;
