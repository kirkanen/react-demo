import React from 'react';
import AddComponent from './AddComponent';
import TableComponent from './TableComponent';
import Contacts from '../assets/data/contacts.json';

export class ContentComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nextId: 21,
      contacts: Contacts,
      sort: false
    }
  }

  onAdd = (contact) => {
    contact.id = this.state.nextId;
    this.setState((prevState, props) => ({
      nextId: this.state.nextId + 1,
      contacts: [...this.state.contacts, contact],
      sort: true
    }));
  };

  onRemove = (id) => {
    let contacts = this.state.contacts.map(a => ({...a}));
    let index = contacts.findIndex(contact => contact.id === id);
    if (index > -1) {
      contacts.splice(index, 1);
    }
    this.setState({
      contacts: contacts
    });
  };

  onSave = (updatedContact) => {
    let contacts = this.state.contacts.map(a => ({...a}));
    contacts.map(contact => {
      if (contact.id === updatedContact.id) {
        contact.name = updatedContact.name;
        contact.email = updatedContact.email;
        contact.phone = updatedContact.phone;
      }
      return contact;
    });
    this.setState({
      contacts: contacts
    });
  };

  onSort = (contacts) => {
    this.setState({
      contacts: contacts,
      sort: false
    });
  }

  render() {
    return (
      <section className="section">
        <h3 className="title">List of Participants</h3>
        <AddComponent onAdd={this.onAdd} />
        <TableComponent contacts={this.state.contacts} sort={this.state.sort} onSave={this.onSave} onRemove={this.onRemove} onSort={this.onSort}/>
      </section>
    );
  }
}

export default ContentComponent;
