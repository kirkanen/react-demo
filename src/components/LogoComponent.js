import React from 'react';

export class LogoComponent extends React.Component {
  render() {
    return (
      <div className="logo-container">
        <div className="logo-spinner">
        <svg width="32" height="32" className="logo">
          <rect x="7" y="7" width="18" height="18" fill="#A1ACB6" />
          <path d="M 11 10 l 0 15" stroke="#FFFFFF" strokeWidth="2" fill="none" />
          <path d="M 12 11 l 3 0" stroke="#FFFFFF" strokeWidth="2" fill="none" />
          <path d="M 16 10 l 0 12" stroke="#FFFFFF" strokeWidth="2" fill="none" />
          <path d="M 17 21 l 3 0" stroke="#FFFFFF" strokeWidth="2" fill="none" />
          <path d="M 21 7 l 0 15" stroke="#FFFFFF" strokeWidth="2" fill="none" />
        </svg>
        </div>
      </div>
    );
  }
};
export default LogoComponent;
