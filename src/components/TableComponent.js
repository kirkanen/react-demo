import React from 'react';
import MaterialIcon from 'react-google-material-icons';
import InputComponent from './InputComponent';

export default class TableComponent extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      contacts: this.props.contacts,
      column: null,
      direction: null
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      contacts: nextProps.contacts
    }, () => {if (nextProps.sort) this.onSort();});
  }

  onSort = (column = null) => {
    let contacts = this.state.contacts.map(a => ({...a}));
    let direction = column ? (this.state.direction && this.state.column === column ? 0 : 1) : this.state.direction;
    if (!column) {
      column = this.state.column;
    }
    if (column) {
      contacts.sort((a, b) => {return direction ?
        ((a[column].toLowerCase() > b[column].toLowerCase()) ? 1 : ((b[column].toLowerCase() > a[column].toLowerCase()) ? -1 : 0)) :
        ((b[column].toLowerCase() > a[column].toLowerCase()) ? 1 : ((a[column].toLowerCase() > b[column].toLowerCase()) ? -1 : 0));
      });
    }
    this.setState({
      contacts: contacts,
      column: column,
      direction: direction
    }, () => this.props.onSort(contacts));
  }

  arrow = (column) => {
    return (
      <span className="arrow">{this.state.column === column ? (this.state.direction ?
        <MaterialIcon icon="arrow_upward" size={16}/> :
        <MaterialIcon icon="arrow_downward" size={16}/>) :
        ''}</span>
    );
  }

  onSave = (contact) => {
    this.props.onSave(contact);
  };

  onRemove = (id) => {
    this.props.onRemove(id);
  };

  render() {
    return (
      <div className = "table">
        <div className="tr">
          <div className={this.state.column === 'name' ? 'th sort' : 'th'} onClick={() => this.onSort('name')}>Name{this.arrow('name')}</div>
          <div className={this.state.column === 'email' ? 'th sort' : 'th'} onClick={() => this.onSort('email')}>E-mail address{this.arrow('email')}</div>
          <div className={this.state.column === 'phone' ? 'th sort' : 'th'} onClick={() => this.onSort('phone')}>Phone number{this.arrow('phone')}</div>
          <div className="th"></div>
        </div>
        {this.state.contacts.map(contact => <TableRow key={contact.id} contact={contact} onSave={this.onSave} onRemove={this.onRemove} />)}
      </div>
    );
  }
};

class TableRow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.contact.id,
      name: {
        name: 'name',
        placeholder: 'Full name',
        value: this.props.contact.name,
        valid: true,
        touched: false,
        validators: ['required']
      },
      email: {
        name: 'email',
        placeholder: 'E-mail address',
        value: this.props.contact.email,
        valid: true,
        touched: false,
        validators: ['required', 'email']
      },
      phone: {
        name: 'phone',
        placeholder: 'Phone number',
        value: this.props.contact.phone,
        valid: true,
        touched: false,
        validators: ['required', 'phone']
      },
      edit: false,
      cancel: false,
      save: false
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    let cancel = nextState.cancel;
    if (cancel) {
      let contact = nextProps.contact,
        name = {...this.state.name},
        email = {...this.state.email},
        phone = {...this.state.phone};
      name.value = contact.name;
      email.value = contact.email;
      phone.value = contact.phone;
      this.setState({
        name: name,
        email: email,
        phone: phone,
        cancel: false
      });
    }
    return cancel || nextState.edit || nextState.save;
  }

  onEdit = () => {
    let name = {...this.state.name};
    let email = {...this.state.email};
    let phone = {...this.state.phone};
    name.valid = true;
    email.valid = true;
    phone.valid = true;
    this.setState({
      name: name,
      email: email,
      phone: phone,
      edit: true
    });
  };

  onRemove = () => {
    this.props.onRemove(this.state.id);
  }

  onCancel = () => {
    this.setState({
      edit: false,
      cancel: true
    });
  };

  onSave = () => {
    this.setState({
      edit: false,
      save: true
    });
    let contact = {
      id: this.state.id,
      name: this.state.name.value,
      email: this.state.email.value,
      phone: this.state.phone.value
    }
    this.props.onSave(contact);
  };

  onUpdate = (target) => {
    let item = {...this.state[target.name]};
    item.value = target.value;
    item.valid = target.valid;
    this.setState({
      [target.name]: item
    });
  };

  formValid() {
    return this.state.name.valid && this.state.email.valid && this.state.phone.valid;
  }

  render() {
    return (
      <div className={this.state.edit ? "tr edit" : "tr"}>
        <div className = "td col1" >
          {this.state.edit ? <InputComponent model={this.state.name} onUpdate={this.onUpdate} /> : this.props.contact.name}
        </div>
        <div className = "td col2" >
          {this.state.edit ? <InputComponent model={this.state.email} onUpdate={this.onUpdate} /> : this.props.contact.email}
        </div>
        <div className = "td col3" >
          {this.state.edit ? <InputComponent model={this.state.phone} onUpdate={this.onUpdate} /> : this.props.contact.phone}
        </div>
        {this.state.edit ?
          <div className = "td col4 edit-column" >
            <button className="cancel" onClick={this.onCancel}>Cancel</button>
            <button disabled={!this.formValid()} onClick={this.onSave}>Save</button>
          </div> :
          <div className = "td col4 edit-column">
            <span className = "icon edit" onClick={this.onEdit}>
              <MaterialIcon icon="mode_edit" size={24} />
            </span>
            <span className = "icon" onClick={this.onRemove}>
              <MaterialIcon icon="delete" size={24} />
            </span>
          </div>
        }
      </div>
    );
  }
}
